# Configuration

# Konocloud API
# KONOCLOUD_API_URI = "http://92.54.15.210:3000/api"
KONOCLOUD_API_URI = "http://api.konodrac.local:3000/api"
KONOCLOUD_API_GROUP = "cn=Administrator,ou=groups,dc=konodrac,dc=com,cn=AudienceFlowReporting,ou=groups,dc=konodrac,dc=com"
KONOCLOUD_API_USER_ID = "admin"

# Mongo host
MONGO_HOST = "mongodb.konodrac.local"
MONGO_PORT = 27017

# Kafka
KAFKA_URI = 'worker1.konodrac.local:6667'

# Slack
#SLACK_TOKEN = "xoxp-18390195648-18396570839-82306411701-9e181dd089d2a0ba9fac7c1ad5265515"
SLACK_TOKEN = "xoxp-323481626258-322929049297-335402142592-df54905f8aaf24114897d4ec877f8f6b"
SLACK_USER_PROCESS_REPORTS = 'aflow-reporting-service ProcessReports'
SLACK_USER_GENERATE_REPORT = 'aflow-reporting-service GenerateReport'

# Paths
BASE_PATH = "/mnt/nfs/aflow_inbox/"
APP_BASE_PATH = "/opt/aflow-reporting-service"

MAX_MONTHS_AGO = 3
